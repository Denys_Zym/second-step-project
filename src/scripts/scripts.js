// Button

const button = document.querySelector(".header__menu-btn");
const menu = document.querySelector(".header__menu-nav");

button.addEventListener("click", () => {
  button.classList.toggle("btn-open-menu");
  button.classList.toggle("btn-close-menu");
  const activeClass = button.classList.contains("btn-close-menu")
    ? "open-menu"
    : "close-menu";
  console.log(activeClass);
  const removeClass = button.classList.contains("btn-close-menu")
    ? "close-menu"
    : "open-menu";
  console.log(removeClass);
  menu.classList.remove(removeClass);
  menu.classList.add(activeClass);
});

// Swith tabs

const tab = document.querySelector(".header__menu-nav");
const tabs = document.querySelectorAll(".header__menu-nav-item");

tab.addEventListener("click", (e) => {
  tabs.forEach((i) => {
    if (e.target === i) {
      e.target.classList.add("item-active");
    } else {
      i.classList.remove("item-active");
    }
  });
});
