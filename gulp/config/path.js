// Get project folder name
// import * as nodePath from 'path';
// const rootFolder = nodePath.basename(nodePath.resolve());

const srcFolder = './src';
const buildFolder = './dist';

export const path = {
	build: {
		js: `${buildFolder}/scripts/`,
		css: `${buildFolder}/styles/`,
		html: `${buildFolder}/`,
		img: `${buildFolder}/img/`,
	},
	src: {
		js: `${srcFolder}/scripts/**/*`,
		scss: `${srcFolder}/scss/main.scss`,
		html: `${srcFolder}/*.html`,
		img: `${srcFolder}/img/**/*`,
	},
	watch: {
		js: `${srcFolder}/scripts/**/*`,
		scss: `${srcFolder}/scss/**/*.scss`,
		html: `${srcFolder}/**/*.html`,
		img: `${srcFolder}/img/**/*`,
	},
	clean: buildFolder,
	buildFolder,
	srcFolder,
	// rootFolder,
}