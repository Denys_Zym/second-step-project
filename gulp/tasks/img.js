import imageMin from "gulp-imagemin";

export const img = () => {
	return app.gulp.src(app.path.src.img)
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "IMAGES",
				message: "Error <%= error.message %>"
			})
		))
		.pipe(imageMin())
		.pipe(app.gulp.dest(app.path.build.img))
}